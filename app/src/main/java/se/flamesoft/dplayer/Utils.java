package se.flamesoft.dplayer;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Point;
import android.view.Display;
import android.view.WindowManager;

/**
 * Created by yanzhang on 10/22/17.
 */

public class Utils {
    public static boolean isOrientationPortrait(Context context) {
        return context.getResources().getConfiguration().orientation
            == Configuration.ORIENTATION_PORTRAIT;
    }

    public static String getDurationText(long ms) {
        int minutes = (int)(ms / 1000 / 60);
        int sec = (int)((ms / 1000) % 60);
        String text = String.format("%02d:%02d", minutes, sec);
        return text;
    }

    public static Point getDisplaySize(Context context) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        int width = display.getWidth();
        int height = display.getHeight();
        return new Point(width, height);
    }

}
